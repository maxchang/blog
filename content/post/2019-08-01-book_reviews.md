---
title: Book Reviews!
date: 2019-08-01
---

I've read a fair amount of books related to professional development that I can post about. I even have some opinions about them! Unfortunately, many of these books I read a couple of years ago so my memory is not so clear. Some of these books were also "read" over audiobook or e-book formats, so they're not easily flippable to refresh my memory. How do people deal with this? But I'll try to remind myself about the content and post some of my enduring thoughts about them. I'll start writing reviews on recently finished books more quickly so that I have a record of my notes available for reference in the future.

As a disclaimer, I expect my reviews to be hastily written notes and thoughts most relevant to me! They may not be comprehensive or go into in-depth analyses on themes or how they related to industry or things like that. For now I'm focusing more on writing anything down at all. This is because I've found that my biggest problem is wanting to only publish work that is "perfect". And perfect is the enemy of the good and all that. So now I'm almost intentionally publishing scribbled notes so that I can get over this fear. Hopefully some people find some of my content interesting, and maybe my writing will even improve through this process!
