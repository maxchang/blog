---
title: Status updates
date: 2019-07-11
---

Like many Scrum teams, we have a daily standup meeting where we give status updates about the previous day. The intention is for the team to keep tabs on what others are working on and help coordinate/resolve dependencies. The reality is that there are often not that many dependencies, and the standup becomes an interruption in an otherwise productive morning. Even though these stand ups only take a few minutes, they can break concentration and require an expensive context switch.

Status updates are still valuable, but there's no real need for them to be done synchronously. So I decided to create a Slack channel for the team to post their status updates. In the short term, I think this will be valuable for the team to record/bookkeep what was done during the day. Typing up a quick, concise status update as something is done is more accurate and easier to recall than in person the next day.

In the medium term, I think this will be valuable for quarterly reviews, as a means to reflect on what was done over the course of a quarter, using notes that you wrote yourself! Sometimes it’s difficult to come up with a list of accomplishments at the end of a quarter, but it’s easier with your status updates in front of you.

I’m also hoping that this may eliminate the standup entirely in the future. I think it accomplishes much of what an in-person standup accomplishes, and if there are any dependencies, individuals can see them in the written status updates and coordinate offline without requiring the rest of the team.

I was inspired to create the new channel after reading [this post](https://mtlynch.io/status-updates-to-nobody/), which suggested that having statuses reported in a separate medium improved the quality of 1:1s with managers, so that the conversation could focus on heavier topics like career growth.

I like this approach, because I get immediate value out of the channel (more accurate updates, no longer having to remember what I did two days ago, etc) without needing buy-in from the rest of the team. I'm hoping that after I start posting my updates and demonstrate its value, this activity allows me to lead by example and start converting other team members. There are some network effects as more people start using the tool, and eventually we may be able to eliminate another meeting!
