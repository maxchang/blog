---
title: Two showers a day
date: 2019-07-14
---

My wife leaves for work around 8:30 in the morning, which gives me 30-45 minutes for a quick workout and shower before catching the train and making my daily standups at 10:45. This routine allows me to leave work at a reasonable time so that I can catch the train and get home (all sweaty) in time for dinner. Life gives you a series of constraints. You have to find what works and make it happen. For me, that's two showers a day.
